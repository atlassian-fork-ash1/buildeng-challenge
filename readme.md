# The BuildEng Puppet Challenge!

## Setup

1. Get yourself setup with Vagrant and Virtualbox, per this guide: https://docs.vagrantup.com/v2/getting-started/index.html
2. Clone this repository to your local machine

## The Challenge

Use puppet to:

* Install Apache2 and PHP
* Configure Apache to serve the phpinfo.php file contained in this repository (correctly interpreted as PHP)

### Tips
* Think about how you're structuring your manifests and modules - points for using modern puppet tools, idioms and style
* How can you automatically verify that your work is correct? Bonus points for implementing a solution
* Consider the layout of the system - this is a simple example, but think about it like any production system that you'd have to setup and maintain.

## Submitting your response

* Tar up your work with the following command:
`tar --exclude .vagrant -zcvf buildeng-challenge-yourname.tar.gz buildeng-challenge/`
* Send us the tarball of your work using the instructions we gave you when we asked you to do the challenge.
* If you forked our repo, please make sure your copy is not publicly accessible.
